<?php

namespace healthCheck;

/**
 * Class CheckResult
 * @package healthCheck
 */
class CheckResult
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $isSuccessful;

    /**
     * @var string
     */
    private $errorMessage;

    /**
     * CheckResult constructor.
     *
     * @param string $name
     * @param bool   $isSuccessful
     * @param string $errorMessage
     */
    public function __construct($name, $isSuccessful, $errorMessage = '')
    {
        $this->name         = $name;
        $this->isSuccessful = $isSuccessful;
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->isSuccessful;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

}