<?php

namespace logger;


use logger\handlers\BasicHandler;
use logger\handlers\LogHandlerInterface;

/**
 * Class LogHandlerFactory
 * @package logger
 */
class LogHandlerFactory
{
    /**
     * @TODO create handlers for all methods
     * Getting log handler
     *
     * @param string $method
     *
     * @return LogHandlerInterface
     */
    public function getHandler(string $method): LogHandlerInterface
    {
        return new BasicHandler();
    }
}