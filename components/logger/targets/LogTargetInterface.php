<?php

namespace logger\targets;

/**
 * Interface LogTargetInterface
 * @package logger\targets
 */
interface LogTargetInterface
{
    /**
     * Export message
     *
     * @param string $content
     *
     * @return
     */
    public function export($content);
}