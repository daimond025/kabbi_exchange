<?php

namespace app\components\orderEventQueue;

use app\components\orderEventQueue\events\OrderBaseEvent;
use app\components\rabbitmq\Amqp;

class OrderEventSender
{

    /**
     * Send event to order queue
     * @param OrderBaseEvent $event
     */
    public function send(OrderBaseEvent $event): void
    {
        /**
         * @var $amqp Amqp
         */
        $amqp = \Yii::$app->get('amqp');
        $amqp->send($event->getEventData(), 'order_' . $event->orderId, '', false);
    }

}