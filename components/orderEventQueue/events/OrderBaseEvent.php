<?php

namespace app\components\orderEventQueue\events;

use yii\base\BaseObject;
use Ramsey\Uuid\Uuid;

abstract class OrderBaseEvent
{
    public const SENDER_SERVICE   = 'exchange_service';
    public const CHANGE_SENDER_ID = 1;
    public const ORDER_EVENT      = 'order_event';
    public $tenantId;
    public $orderId;
    public $senderId;
    public $lang;
    public $params;

    public function __construct(int $tenantId, int $orderId, int $senderId, array $params, $lang = 'en')
    {
        $this->tenantId = $tenantId;
        $this->orderId = $orderId;
        $this->senderId = $senderId;
        $this->params = $params;
        $this->lang = $lang;
    }

    /**
     * Get event data
     * @return mixed
     */
    abstract public function getEventData(): array;

    /**
     * Generate uuid
     * @return string
     */
    protected function generateUuid():string
    {
        return Uuid::uuid4()->toString();
    }
}