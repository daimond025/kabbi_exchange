<?php

namespace app\components\orderEventQueue\events;

use yii\base\BaseObject;

class OrderUpdateEvent extends OrderBaseEvent
{
    public const COMMAND = 'update_order_data';

    /**
     * Get event data
     * @return array
     */
    public function getEventData(): array
    {
        return [
            'uuid'             => parent::generateUuid(),
            'type'             => parent::ORDER_EVENT,
            'timestamp'        => time(),
            'sender_service'   => parent::SENDER_SERVICE,
            'command'          => self::COMMAND,
            'order_id'         => $this->orderId,
            'tenant_id'        => $this->tenantId,
            'change_sender_id' => $this->senderId,
            'last_update_time' => time(),
            'lang'             => $this->lang,
            'params'           => $this->params,
        ];
    }


}