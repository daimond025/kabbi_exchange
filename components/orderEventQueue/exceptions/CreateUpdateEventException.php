<?php

namespace app\components\orderEventQueue\exceptions;

use yii\base\Exception;

/**
 * Class CreateUpdateEventException
 */
class CreateUpdateEventException extends Exception
{

}