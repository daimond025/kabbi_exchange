<?php

namespace app\components\orderEventQueue\exceptions;

use yii\base\Exception;

/**
 * Class GetUpdateEventResultException
 */
class GetUpdateEventResultException extends Exception
{

}