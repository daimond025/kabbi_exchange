<?php

namespace app\components\orderEventQueue\exceptions;

use yii\base\Exception;

/**
 * Class QueueIsNotExistsException
 */
class QueueIsNotExistsException extends Exception
{

}