<?php


namespace app\components\xml;

use SimpleXMLElement;
use yii\base\BaseObject;
use yii\helpers\ArrayHelper;
use yii\web\RequestParserInterface;

class XmlParser extends BaseObject implements RequestParserInterface
{
    /**
     * @inheritdoc
     */
    public function parse($rawBody, $contentType): array
    {
        $xml = simplexml_load_string($rawBody, 'SimpleXMLElement', LIBXML_NOCDATA);

        $result = $this->convertXmlToArray($xml);
        $rootAttributes = $this->getAttributes($xml);

        if (!empty($rootAttributes)) {
            $result['attributes'] = $this->getAttributes($xml);
        }

        return $result;
    }

    /**
     * Converts XML document to array.
     * @param SimpleXMLElement $xml xml to process.
     * @return array|string XML array representation.
     */
    protected function convertXmlToArray(SimpleXMLElement $xml)
    {
        if ($xml->count() > 0) {
            $result = [];

            /** @var SimpleXMLElement $value */
            foreach ($xml as $key => $value) {
                if ($value->count() > 0) {
                    if (!array_key_exists($key, $result)) {
                        $result[$key] = [];
                    }

                    foreach ($value as $k => $v) {
                        if (array_key_exists($k, $result[$key])) {
                            if (!is_array($result[$key][$k]) ||
                                (is_array($result[$key][$k]) && array_key_exists('attributes', $result[$key][$k])) ||
                                (is_array($result[$key][$k]) && !ArrayHelper::isIndexed($result[$key][$k]))
                            ) {
                                $singleValue = $result[$key][$k];
                                $result[$key][$k] = [];
                                $result[$key][$k][] = $singleValue;

                            }
                            $result[$key][$k][] = $this->convertXmlToArray($v);
                        } else {
                            $result[$key][$k] = $this->convertXmlToArray($v);
                        }
                    }
                } else {
                    $attributes = $this->getAttributes($value);

                    if (array_key_exists($key, $result)) {
                        if (!is_array($result[$key])) {
                            $singleValue = $result[$key];
                            $result[$key] = [];
                            $result[$key][] = $singleValue;
                        }

                        end($result[$key]);
                        $itemKey = key($result[$key]) + 1;

                        $result[$key][] = $this->getValueFormat($attributes, (string)$xml->$key[$itemKey]);
                    } else {
                        $result[$key] = $this->getValueFormat($attributes, (string)$xml->$key);
                    }
                }
            }
        } else {
            $attributes = $this->getAttributes($xml);
            $itemValue = (string)$xml;

            $result = $this->getValueFormat($attributes, $itemValue);
        }

        return $result;
    }

    private function getAttributes(SimpleXMLElement $element): array
    {
        $attributes = $element->attributes();

        $result = (array)$attributes;

        return $result['@attributes'] ?? [];
    }

    /**
     * @param array $attributes
     * @param string $value
     * @return array|string
     */
    private function getValueFormat(array $attributes, string $value)
    {
        if (!empty($attributes)) {
            return [
                'attributes' => $attributes,
                'value'      => $value,
            ];
        }

        return $value;
    }
}