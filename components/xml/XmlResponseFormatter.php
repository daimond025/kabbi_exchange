<?php


namespace app\components\xml;


use app\components\xml\builder\interfaces\IXmlBuilder;
use app\components\xml\builder\XmlBuilder;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\base\BaseObject;
use yii\web\Response;
use yii\web\ResponseFormatterInterface;

class XmlResponseFormatter extends BaseObject implements ResponseFormatterInterface
{
    /**
     * @var string the Content-Type header for the response
     */
    public $contentType = 'application/xml';
    /** @var  XmlBuilder */
    private $xmlBuilder;

    public function init()
    {
        parent::init();

        if (empty($this->xmlBuilder)) {
            throw new InvalidConfigException('The xmlbulder must be set');
        }
    }

    /**
     * Formats the specified response.
     * @param Response $response the response to be formatted.
     */
    public function format($response)
    {
        $charset = $this->xmlBuilder->encoding === null ? $response->charset : $this->xmlBuilder->encoding;
        if (stripos($this->contentType, 'charset') === false) {
            $this->contentType .= '; charset=' . $charset;
        }
        $response->getHeaders()->set('Content-Type', $this->contentType);
        if ($response->data !== null) {
            $response->content = $this->xmlBuilder->build($response->data);
        }
    }

    /**
     * @param mixed $xmlBuilder
     */
    public function setXmlBuilder($xmlBuilder)
    {
        $this->xmlBuilder = Yii::createObject($xmlBuilder);

        if (!($this->xmlBuilder instanceof IXmlBuilder)) {
            throw new InvalidParamException('Xml builder must be instance of app\components\xml\builder\interfaces\IXmlBuilder');
        }
    }
}