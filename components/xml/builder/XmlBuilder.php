<?php


namespace app\components\xml\builder;


use app\components\xml\builder\interfaces\IXmlBuilder;
use DOMDocument;
use DOMElement;
use DOMText;
use yii\base\Arrayable;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class XmlBuilder implements IXmlBuilder
{
    /**
     * @var string the XML version
     */
    public $version = '1.0';
    /**
     * @var string the XML encoding. If not set, it will use the value of [[Response::charset]].
     */
    public $encoding = 'utf-8';
    /**
     * @var string the name of the root element. If set to false, null or is empty then no root tag should be added.
     */
    public $rootTag = 'response';
    /**
     * <Cars>
     *  <Car>Ford Mondeo</Car>
     *  <Car>Lada Granta</Car>
     *  <Car>Nissan Almera</Car>
     * </Cars>
     * @var bool
     */
    public $singularizeItemTagByParent = true;
    /** @var array [['name', 'value']] Example: [['v', '1.5.14'], ['test', '234']] */
    public $rootAttributes = [];
    /**
     * @var bool whether to interpret objects implementing the [[\Traversable]] interface as arrays.
     * Defaults to `true`.
     */
    public $useTraversableAsArray = true;
    /**
     * @var string the name of the elements that represent the array elements with numeric keys.
     */
    public $itemTag = 'item';
    /**
     * @var bool if object tags should be added
     */
    public $useObjectTags = true;

    /**
     * @param array $data
     * @return string
     */
    public function build(array $data): string
    {
        $dom = new DOMDocument($this->version, $this->encoding);
        if (!empty($this->rootTag)) {
            $root = new DOMElement($this->rootTag);
            $dom->appendChild($root);
            if (is_array($this->rootAttributes) && !empty($this->rootAttributes)) {
                foreach ($this->rootAttributes as $attribute) {
                    $root->setAttribute($attribute[0], $attribute[1]);
                }
            }
            $this->buildXml($root, $data);
        } else {
            $this->buildXml($dom, $data);
        }

        return $dom->saveXML();
    }

    /**
     * @param DOMElement|DOMDocument $element
     * @param mixed $data
     */
    protected function buildXml($element, $data)
    {
        if (is_array($data) ||
            ($data instanceof \Traversable && $this->useTraversableAsArray && !$data instanceof Arrayable)
        ) {
            foreach ($data as $name => $value) {
                if (is_int($name) && is_object($value)) {
                    $this->buildXml($element, $value);
                } else {
                    if (is_int($name)) {
                        if (!isset($value['tag'])) {
                            $elementName = $this->singularizeItemTagByParent ? Inflector::singularize($element->tagName) : $this->itemTag;
                        } else {
                            $elementName = $value['tag'];
                        }
                    } else {
                        $elementName = $name;
                    }

                    $child = new DOMElement($elementName);
                    $element->appendChild($child);

                    if (is_array($value) || is_object($value)) {
                        if (array_key_exists('tag', $value)) {
                            if (array_key_exists('attributes', $value) && is_array($value['attributes'])) {
                                foreach ($value['attributes'] as $key => $attribute) {
                                    $child->setAttribute($key, $attribute);
                                }
                            }
                            $tagValue = $value['value'] ?? null;
                            if (is_array($tagValue)) {
                                $this->buildXml($child, $tagValue);
                            } else {
                                $child->appendChild(new DOMText($this->formatScalarValue($tagValue)));
                            }
                        } else {
                            $this->buildXml($child, $value);
                        }
                    } else {
                        $child->appendChild(new DOMText($this->formatScalarValue($value)));
                    }
                }
            }
        } elseif (is_object($data)) {
            if ($this->useObjectTags) {
                $child = new DOMElement(StringHelper::basename(get_class($data)));
                $element->appendChild($child);
            } else {
                $child = $element;
            }
            if ($data instanceof Arrayable) {
                $this->buildXml($child, $data->toArray());
            } else {
                $array = [];
                foreach ($data as $name => $value) {
                    $array[$name] = $value;
                }
                $this->buildXml($child, $array);
            }
        } else {
            $element->appendChild(new DOMText($this->formatScalarValue($data)));
        }
    }

    /**
     * Formats scalar value to use in XML text node
     *
     * @param int|string|bool $value
     * @return string
     */
    protected function formatScalarValue($value): string
    {
        if ($value === true) {
            return 'true';
        }

        if ($value === false) {
            return 'false';
        }

        return (string)$value;
    }
}