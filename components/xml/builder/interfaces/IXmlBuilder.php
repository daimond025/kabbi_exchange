<?php


namespace app\components\xml\builder\interfaces;


interface IXmlBuilder
{
    public function build(array $data): string;
}