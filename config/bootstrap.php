<?php


\Yii::setAlias('logger', dirname(__DIR__) . '/components/logger');
\Yii::setAlias('healthCheck', dirname(__DIR__) . '/components/healthCheck');

\logger\ApiLogger::setupStartTimeForStatistic();