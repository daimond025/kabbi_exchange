<?php

$db = [
    'dbMain' => [
        'class'               => yii\db\Connection::class,
        'charset'             => 'utf8',
        'tablePrefix'         => 'tbl_',
        'dsn'                 => getenv('DB_MAIN_DSN'),
        'username'            => getenv('DB_MAIN_USERNAME'),
        'password'            => getenv('DB_MAIN_PASSWORD'),
        'enableSchemaCache'   => getenv('DB_MAIN_SCHEMA_CACHE_ENABLE') ? true : false,
        'schemaCacheDuration' => (int)getenv('DB_MAIN_SCHEMA_CACHE_DURATION'),
    ],


    'redisCache' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_MAIN'),
    ],


    'redisMainWorkers' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_WORKERS'),
    ],

    'redisMainOrdersActive' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ORDERS_ACTIVE'),
    ],

    'redisMainOrderEvent' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_ORDER_EVENT'),
    ],

    'redisMainCheckStatus' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_MAIN_HOST'),
        'port'     => getenv('REDIS_MAIN_PORT'),
        'database' => getenv('REDIS_MAIN_DATABASE_CHECK_STATUS'),
    ],

    'redisCacheCheckStatus' => [
        'class'    => yii\redis\Connection::class,
        'hostname' => getenv('REDIS_CACHE_HOST'),
        'port'     => getenv('REDIS_CACHE_PORT'),
        'database' => getenv('REDIS_CACHE_DATABASE_CHECK_STATUS'),
    ],

];

if ((bool)getenv('DB_MAIN_SLAVE_ENABLE')) {
    $db['dbMain']['slaveConfig'] = [
        'username' => getenv('DB_MAIN_SLAVE_USERNAME'),
        'password' => getenv('DB_MAIN_SLAVE_PASSWORD'),
        'charset'  => 'utf8',
    ];

    $db['dbMain']['slaves'] = [
        ['dsn' => getenv('DB_MAIN_SLAVE_DSN')],
    ];
}

return $db;