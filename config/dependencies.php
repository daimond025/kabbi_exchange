<?php

$version  = require __DIR__ . '/version.php';
$identity = getenv('SYSLOG_IDENTITY') . '-' . $version;

\Yii::$container->set(
    \logger\targets\LogTargetInterface::class,
    \logger\targets\SyslogTarget::class,
    [$identity]
);

\Yii::$container->set(
    \logger\LogHandlerFactory::class
);