<?php

use app\helpers\TenantRequestDataHelper;
use app\repositories\ActiveDriverRepository;

return [
    'definitions' => [
    ],
    'singletons'  => [
        app\repositories\interfaces\IDocumentsRepository::class    => app\repositories\DocumentsRepository::class,
        app\repositories\interfaces\IActiveOrders ::class          => app\repositories\ActiveOrdersRepository::class,
        app\repositories\interfaces\IActiveDriverRepository::class => function () {
            return new ActiveDriverRepository(TenantRequestDataHelper::getId());
        },
    ],
];