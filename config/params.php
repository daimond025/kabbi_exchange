<?php

return [
    // Position ID for taxi driver of "tbl_position"
    'taxi_position_id'        => 1,
    //Field "code" of "tbl_document" for driver license
    'driver_license_doc_code' => 7,
    'version'                 => require 'version.php'
];
