<?php
$baseParams = require __DIR__ . '/baseApiUrl.php';

return [
    /*
    //DRIVERS
    //Экспорт водителей
    "GET $baseParams/drivers"                             => "<module>/driver/",
    //Изменение статуса
    "POST $baseParams/drivers/callsign/<callsign>/status" => "<module>/driver/send-status",
    //Отправка координат
    "GET $baseParams/drivers/sendlocation"                => "<module>/driver/send-locations",
    */

    //ORDERS
    //sell
    //Добавить
    "POST $baseParams/orders/add"               => '<module>/order-selling/add',
    //Редактировать
    "POST $baseParams/orders/update/<order_id>" => '<module>/order-selling/update',
    //Получить информацию
    "POST $baseParams/orders/info/<order_id>"   => '<module>/order-selling/info',
    //Удалить
    "POST $baseParams/orders/delete/<order_id>" => '<module>/order-selling/delete',
    //Изменение статуса
    "GET $baseParams/orderstatus"               => '<module>/order-selling/change-status',

    /*
    //buy
    //Предложение заказа
    "POST $baseParams/requestcar"                         => "<module>/order-buy/offer-order",
    //Закрепление заказа
    "POST $baseParams/setcar"                             => "<module>/order-buy/set-car",
    //Сообщение о готовности водителя выполнить заказ
    "POST $baseParams/order-buy/confirm-order"            => "<module>/order-buy/confirm-order",
    //Обновление статуса заказа
    "POST $baseParams/order-buy/change-status"            => "<module>/order-buy/change-status",
    //Отмана заказа
    "GET $baseParams/cancelrequest"                       => "<module>/order-buy/reject",
    */
    'GET <action>' => 'site/<action>'
];