<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$logVars = array_merge(
    ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION', '_SERVER'],
    require __DIR__ . '/logVarsFilter.php'
);

$config = [
    'id'                  => 'api_exchange',
    'basePath'            => dirname(__DIR__),
    'vendorPath'          => dirname(__DIR__, 2) . '/vendor',
    'bootstrap'           => ['log'],
    'container'           => require __DIR__ . '/dic.php',
    'controllerNamespace' => 'app\controllers',
    'components'          => [
        'assetManager' => [
            'basePath' => '@webroot/assets',
            'baseUrl'  => '@web/assets',
        ],
        'i18n'         => [
            'translations' => [
                '*'    => [
                    'class'            => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                ],
                'app*' => [
                    'class'            => yii\i18n\DbMessageSource::class,
                    'forceTranslation' => true,
                ],
            ],
        ],
        'request'      => [
            'baseUrl'              => '',
            'cookieValidationKey'  => getenv('COOKIE_VALIDATION_KEY'),
            'enableCsrfValidation' => false,
            'parsers'              => [
                'text/xml'        => app\components\xml\XmlParser::class,
                'application/xml' => app\components\xml\XmlParser::class,
            ],
        ],
        'apiLogger'    => logger\ApiLogger::class,
        'response'     => [
            'format'       => [
                yii\web\Response::FORMAT_XML,
                yii\web\Response::FORMAT_JSON,
                yii\web\Response::FORMAT_RAW
            ],
            'charset'      => 'UTF-8',
            'formatters'   => [
                yii\web\Response::FORMAT_XML => [
                    'class'      => app\components\xml\XmlResponseFormatter::class,
                    'xmlBuilder' => app\components\xml\builder\XmlBuilder::class,
                ],
            ],
            'class'        => \yii\web\Response::class,
            'on afterSend' => function () {
                /** @var $apiLogger \logger\ApiLogger */
                $apiLogger = \Yii::$app->apiLogger;
                $apiLogger->setRequest(\Yii::$app->request);
                $apiLogger->setResponse(\Yii::$app->response);
                $apiLogger->export();
            },
        ],
        'cache'        => [
            'class'     => yii\redis\Cache::class,
            'redis'     => $db['redisCache'],
            'keyPrefix' => getenv('REDIS_CACHE_KEY_PREFIX'),
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer'       => [
            'class'            => yii\swiftmailer\Mailer::class,
            'useFileTransport' => false,
            'transport'        => [
                'class'      => 'Swift_SmtpTransport',
                'host'       => getenv('MAIL_SUPPORT_HOST'),
                'port'       => getenv('MAIL_SUPPORT_PORT'),
                'username'   => getenv('MAIL_SUPPORT_USERNAME'),
                'password'   => getenv('MAIL_SUPPORT_PASSWORD'),
                'encryption' => getenv('MAIL_SUPPORT_ENCRYPTION'),
            ],
        ],

        'log'                      => [
            'traceLevel' => 3,
            'targets'    => [
                [
                    'class'   => yii\log\FileTarget::class,
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                ],
                [
                    'class'   => yii\log\EmailTarget::class,
                    'except'  => ['yii\debug\Module::checkAccess'],
                    'mailer'  => 'mailer',
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'message' => [
                        'from'    => [getenv('MAIL_SUPPORT_USERNAME')],
                        'to'      => [getenv('MAIL_SUPPORT_USERNAME')],
                        'subject' => getenv('MAIL_SUPPORT_SUBJECT'),
                    ],
                ],
                [
                    'class'   => 'notamedia\sentry\SentryTarget',
                    'dsn'     => getenv('SENTRY_DSN'),
                    'levels'  => ['error', 'warning'],
                    'logVars' => $logVars,
                    'except'  => [
                        'yii\web\HttpException:400',
                        'yii\web\HttpException:401',
                        'yii\web\HttpException:403',
                        'yii\web\HttpException:404',
                    ],
                    'clientOptions' => [
                        'environment' => getenv('ENVIRONMENT_NAME'),
                    ],
                ],
            ],
        ],
        'db'                       => $db['dbMain'],
        'redis_workers'            => $db['redisMainWorkers'],
        'redis_orders'             => $db['redisMainOrdersActive'],
        'redis_main_check_status'  => $db['redisMainCheckStatus'],
        'redis_cache_check_status' => $db['redisCacheCheckStatus'],
        'urlManager'               => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => require('url_rules.php'),
            'normalizer'      => [
                'class'  => yii\web\UrlNormalizer::class,
                'action' => yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY,
            ],
        ],
        'httpClient'               => [
            'class'      => yii\httpclient\Client::class,
            'formatters' => [
                yii\httpclient\Client::FORMAT_XML => [
                    'class'      => app\components\xml\XmlResponseFormatter::class,
                    'xmlBuilder' => app\components\xml\builder\XmlBuilder::class,
                ],
            ],
        ],
        'xmlBuilder'               => [
            'class' => app\components\xml\builder\XmlBuilder::class,
        ],
        'amqp'                     => [
            'class'    => app\components\rabbitmq\Amqp::class,
            'host'     => getenv('RABBITMQ_MAIN_HOST'),
            'port'     => getenv('RABBITMQ_MAIN_PORT'),
            'user'     => getenv('RABBITMQ_MAIN_USER'),
            'password' => getenv('RABBITMQ_MAIN_PASSWORD'),
        ],
        'orderEventSender'         => [
            'class' => app\components\orderEventQueue\OrderEventSender::class,
        ],
    ],
    'params'              => $params,
    'modules'             => [
        'upup' => [
            'class'           => app\modules\upup\Module::class,
            'protocolVersion' => '1.5.14',
            'filters'         => [
                app\filters\TenantFilter::class
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class'      => yii\debug\Module::class,
        'allowedIPs' => ['192.168.1.*'],
    ];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class'      => yii\gii\Module::class,
        'allowedIPs' => ['192.168.1.*'],
    ];
}

return $config;
