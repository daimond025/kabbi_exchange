<?php


namespace app\controllers;


use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        unset($behaviors['rateLimiter']);
        return array_merge($behaviors, [
            'contentNegotiator' => [
                'class'   => ContentNegotiator::className(),
                'formats' => [
                    'application/xml'  => Response::FORMAT_XML,
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ]);
    }
}