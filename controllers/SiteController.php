<?php

namespace app\controllers;

use yii\web\Controller;
use yii\web\ErrorAction;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\base\Module;
use yii\base\Application;
use yii\helpers\Html;
use healthCheck\CheckResult;
use healthCheck\checks\BaseCheck;
use healthCheck\checks\CustomCheck;
use healthCheck\checks\MySqlCheck;
use healthCheck\checks\RedisCheck;
use healthCheck\HealthCheckService;


class SiteController extends Controller
{
    public const SERVICE_OPERATIONAL     = 'SERVICE OPERATIONAL';
    public const SERVICE_NOT_OPERATIONAL = 'SERVICE NOT OPERATIONAL';

    public const STATUS_OK    = 'ok';
    public const STATUS_ERROR = 'error';

    public const STATUS_LABELS = [
        self::STATUS_OK    => '[ OK    ]',
        self::STATUS_ERROR => '[ ERROR ]',
    ];

    public const MIME_TYPE_TEXT_PLAIN = 'text/plain';

    /**
     * @var Application
     */
    private $application;


    /**
     * ApiController constructor.
     *
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->application = \Yii::$app;
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];


    }

    /**
     * @throws NotFoundHttpException
     */
    public function actionError(): void
    {
        throw new NotFoundHttpException('The request page does not exist');
    }

    public function actionPing(): int
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        return 1;
    }

    public function actionVersion()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        $result = implode(PHP_EOL, [$this->getApiVersion(), $this->getPHPVersion()]);

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }

    public function actionStatus()
    {
        \Yii::$app->response->format = Response::FORMAT_RAW;
        $status = self::STATUS_OK;

        $lines = [$this->getApiVersion(), $this->getPHPVersion(), ''];

        $checkResults = (new HealthCheckService($this->getHealthChecks()))->getCheckResults();

        foreach ($checkResults as $checkResult) {
            /* @var $checkResult CheckResult */
            if ($checkResult->isSuccessful()) {
                $lines[] = self::STATUS_LABELS[self::STATUS_OK] . " {$checkResult->getName()}";
            } else {
                $lines[] = self::STATUS_LABELS[self::STATUS_ERROR] . " {$checkResult->getName()} ({$checkResult->getErrorMessage()})";
                $status = self::STATUS_ERROR;
            }
        }

        $lines[] = '';
        $lines[] = $status === self::STATUS_OK ? self::SERVICE_OPERATIONAL : self::SERVICE_NOT_OPERATIONAL;

        $result = implode(PHP_EOL, $lines);

        return $this->isTextFormatRequested()
            ? $result : Html::tag('pre', Html::encode($result),
                ['style' => 'word - wrap: break-word; white - space: pre - wrap']);
    }


    /**
     * Get health checks
     * @return BaseCheck[]
     */
    private function getHealthChecks(): array
    {
        $application = $this->application;
        return [
            /**
             *
             */
            new MySqlCheck('database "db"', $application->get('db')),
            new RedisCheck('database "redis"', $application->get('redis_main_check_status')),
            new RedisCheck('database "redis (cache)"', $application->get('redis_cache_check_status')),
            new CustomCheck('service "rabbitmq"',
                function () use ($application) {
                    $application->get('amqp');
                }),

        ];
    }


    /**
     * Get api version
     * @return string
     */
    private function getApiVersion(): string
    {
        $application = \Yii::$app->id;
        $version = \Yii::$app->params['version'];

        return "{$application} v{$version}";
    }

    /**
     * Get php version
     * @return string
     */
    private function getPHPVersion(): string
    {
        $php = PHP_VERSION;

        return "work on php v{$php}";
    }

    /**
     * Is text format requested
     * @return bool
     */
    private function isTextFormatRequested(): bool
    {
        return array_key_exists(self::MIME_TYPE_TEXT_PLAIN, $this->application->request->acceptableContentTypes);
    }
}
