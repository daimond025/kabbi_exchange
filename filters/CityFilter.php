<?php


namespace app\filters;


use app\filters\interfaces\IFilter;
use Yii;
use yii\web\NotFoundHttpException;

class CityFilter implements IFilter
{
    /**
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $cityId = Yii::$app->request->get('city_id');

        if (empty($cityId)) {
            throw new NotFoundHttpException('Unknown the city_id');
        }
    }
}