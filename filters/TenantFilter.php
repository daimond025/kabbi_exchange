<?php


namespace app\filters;


use app\filters\interfaces\IFilter;
use app\helpers\TenantRequestDataHelper;
use yii\web\NotFoundHttpException;

class TenantFilter implements IFilter
{
    /**
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $domainName = TenantRequestDataHelper::getDomain();

        if (!$domainName) {
            throw new NotFoundHttpException('Unknown the tenant');
        }

        $tenantId = TenantRequestDataHelper::getId($domainName);

        if (!$tenantId) {
            throw new NotFoundHttpException('Unknown the tenant_id');
        }
    }
}