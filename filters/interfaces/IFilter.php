<?php


namespace app\filters\interfaces;


interface IFilter
{
    public function run();
}