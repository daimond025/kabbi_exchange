<?php


namespace app\helpers;


class PhoneHelper
{
    public static function cleanValue(string $value): string
    {
        return preg_replace("/[^0-9]/", '', $value);
    }
}