<?php


namespace app\helpers;


use app\repositories\exceptions\NotFoundException;
use app\repositories\TenantRepository;
use Yii;

class TenantRequestDataHelper
{
    public static function getId(?string $domainName = null)
    {
        $domainName = $domainName ?? self::getDomain();

        return Yii::$app->cache->getOrSet($domainName, function () use ($domainName) {
            /** @var TenantRepository $tenantRepository */
            $tenantRepository = Yii::createObject(TenantRepository::class);
            try {
                return $tenantRepository->getIdByDomain($domainName);
            } catch (NotFoundException $e) {
                return false;
            }
        });
    }

    /**
     * @return string
     */
    public static function getDomain(): ?string
    {
        return Yii::$app->request->get('domain');
    }
}