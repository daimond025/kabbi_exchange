<?php

/**
 * Debug function with die() after
 * dd($var);
 */
function dd($var, $depth = 10, $highlight = true)
{
    dump($var, $depth, $highlight);
    die();
}

function dump($var, $depth = 10, $highlight = true)
{
    \yii\helpers\VarDumper::dump($var, $depth, $highlight);
}

function array_map_assoc($callback, $array)
{
    $r = [];
    foreach ($array as $key => $value)
        $r[$key] = $callback($key, $value);
    return $r;
}