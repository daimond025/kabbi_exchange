<?php

namespace app\models\city;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%city}}".
 *
 * @property integer $city_id
 * @property integer $republic_id
 * @property string $name
 * @property string $shortname
 * @property string $lat
 * @property string $lon
 * @property string $fulladdress
 * @property string $fulladdress_reverse
 * @property string $sort
 * @property string $city_polygon
 * @property string $name_az
 * @property string $name_en
 * @property string $name_de
 * @property string $name_sr
 * @property string $fulladdress_az
 * @property string $fulladdress_en
 * @property string $fulladdress_de
 * @property string $fulladdress_sr
 * @property integer $search
 * @property string $name_ro
 * @property string $fulladdress_ro
 * @property string $name_uz
 * @property string $fulladdress_uz
 * @property string $name_fi
 * @property string $fulladdress_fi
 * @property string $name_fa
 * @property string $fulladdress_fa
 *
 */
class City extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['republic_id', 'name'], 'required'],
            [['republic_id', 'search'], 'integer'],
            [['city_polygon'], 'string'],
            [
                [
                    'name',
                    'shortname',
                    'lat',
                    'lon',
                    'name_az',
                    'name_en',
                    'name_de',
                    'name_sr',
                    'fulladdress_az',
                    'fulladdress_en',
                    'fulladdress_de',
                    'fulladdress_sr',
                    'name_fi',
                    'name_fa',
                ],
                'string',
                'max' => 100,
            ],
            [['fulladdress', 'fulladdress_reverse'], 'string', 'max' => 200],
            [['sort'], 'string', 'max' => 45],
            [
                ['name_ro', 'fulladdress_ro', 'name_uz', 'fulladdress_uz', 'fulladdress_fi', 'fulladdress_fa'],
                'string',
                'max' => 255,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id'             => 'City ID',
            'republic_id'         => 'Republic ID',
            'name'                => 'Name',
            'shortname'           => 'Shortname',
            'lat'                 => 'Lat',
            'lon'                 => 'Lon',
            'fulladdress'         => 'Fulladdress',
            'fulladdress_reverse' => 'Fulladdress Reverse',
            'sort'                => 'Sort',
            'city_polygon'        => 'City Polygon',
            'name_az'             => 'Name Az',
            'name_en'             => 'Name En',
            'name_de'             => 'Name De',
            'name_sr'             => 'Name Sr',
            'fulladdress_az'      => 'Fulladdress Az',
            'fulladdress_en'      => 'Fulladdress En',
            'fulladdress_de'      => 'Fulladdress De',
            'fulladdress_sr'      => 'Fulladdress Sr',
            'search'              => 'Search',
            'name_ro'             => 'Name Ro',
            'fulladdress_ro'      => 'Fulladdress Ro',
            'name_uz'             => 'Name Uz',
            'fulladdress_uz'      => 'Fulladdress Uz',
            'name_fi'             => 'Name Fi',
            'fulladdress_fi'      => 'Fulladdress Fi',
            'name_fa'             => 'Name Fa',
            'fulladdress_fa'      => 'Fulladdress Fa',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepublic()
    {
        return $this->hasOne(Republic::className(), ['republic_id' => 'republic_id']);
    }
}
