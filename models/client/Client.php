<?php

namespace app\models\client;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%client}}".
 *
 * @property integer $client_id
 * @property integer $tenant_id
 * @property integer $city_id
 * @property string $photo
 * @property string $last_name
 * @property string $name
 * @property string $second_name
 * @property string $email
 * @property integer $black_list
 * @property integer $priority
 * @property string $create_time
 * @property integer $active
 * @property integer $success_order
 * @property integer $fail_worker_order
 * @property integer $fail_client_order
 * @property integer $fail_dispatcher_order
 * @property string $birth
 * @property integer $password
 * @property string $device
 * @property string $device_token
 * @property string $lang
 * @property string $description
 * @property string $auth_key
 * @property integer $active_time
 *
 * @property ClientPhone $phone
 */
class Client extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%client}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tenant_id', 'city_id'], 'required'],
            [
                [
                    'tenant_id',
                    'city_id',
                    'black_list',
                    'priority',
                    'active',
                    'success_order',
                    'fail_worker_order',
                    'fail_client_order',
                    'fail_dispatcher_order',
                    'password',
                    'active_time',
                ],
                'integer',
            ],
            [['birth'], 'safe'],
            [['device', 'device_token'], 'string'],
            [['photo', 'description', 'auth_key'], 'string', 'max' => 255],
            [['last_name', 'name', 'second_name', 'email'], 'string', 'max' => 45],
            [['lang'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'client_id'             => 'Client ID',
            'tenant_id'             => 'Tenant ID',
            'city_id'               => 'City ID',
            'photo'                 => 'Photo',
            'last_name'             => 'Last Name',
            'name'                  => 'Name',
            'second_name'           => 'Second Name',
            'email'                 => 'Email',
            'black_list'            => 'Black List',
            'priority'              => 'Priority',
            'create_time'           => 'Create Time',
            'active'                => 'Active',
            'success_order'         => 'Success Order',
            'fail_worker_order'     => 'Fail Worker Order',
            'fail_client_order'     => 'Fail Client Order',
            'fail_dispatcher_order' => 'Fail Dispatcher Order',
            'birth'                 => 'Birth',
            'password'              => 'Password',
            'device'                => 'Device',
            'device_token'          => 'Device Token',
            'lang'                  => 'Lang',
            'description'           => 'Description',
            'auth_key'              => 'Auth Key',
            'active_time'           => 'Active Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhone()
    {
        return $this->hasOne(ClientPhone::className(), ['client_id' => 'client_id']);
    }

    public function getFullName(): string
    {
        return trim($this->last_name . ' ' . $this->name . ' ' . $this->second_name);
    }
}
