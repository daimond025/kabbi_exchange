<?php

namespace app\models\order;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%order_has_option}}".
 *
 * @property string $order_id
 * @property integer $option_id
 *
 */
class OrderHasOption extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order_has_option}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'option_id'], 'required'],
            [['order_id', 'option_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id'  => Yii::t('order', 'Order ID'),
            'option_id' => Yii::t('order', 'Option ID'),
        ];
    }
}
