<?php

namespace app\models\worker;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%document}}".
 *
 * @property integer $document_id
 * @property string $name
 * @property integer $code
 *
 * @property WorkerHasDocument[] $workerHasDocuments
 */
class Document extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['code'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'name'        => 'Name',
            'code'        => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['document_id' => 'document_id']);
    }
}
