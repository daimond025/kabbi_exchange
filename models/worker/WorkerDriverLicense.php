<?php

namespace app\models\worker;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_driver_license}}".
 *
 * @property integer $id
 * @property integer $worker_document_id
 * @property string $series
 * @property string $number
 * @property string $category
 * @property string $start_date
 *
 * @property WorkerHasDocument $workerDocument
 */
class WorkerDriverLicense extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_driver_license}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_document_id'], 'required'],
            [['worker_document_id'], 'integer'],
            [['start_date'], 'safe'],
            [['series', 'number'], 'string', 'max' => 20],
            [['category'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                 => 'ID',
            'worker_document_id' => 'Worker Document ID',
            'series'             => 'Series',
            'number'             => 'Number',
            'category'           => 'Category',
            'start_date'         => 'Start Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDocument()
    {
        return $this->hasOne(WorkerHasDocument::className(), ['id' => 'worker_document_id']);
    }
}
