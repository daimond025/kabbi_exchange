<?php

namespace app\models\worker;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_has_document}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $document_id
 * @property integer $has_position_id
 *
 * @property WorkerDriverLicense[] $workerDriverLicenses
 * @property WorkerHasPosition $hasPosition
 * @property Document $document
 * @property Worker $worker
 */
class WorkerHasDocument extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'document_id'], 'required'],
            [['worker_id', 'document_id', 'has_position_id'], 'integer'],
            [
                ['worker_id', 'document_id', 'has_position_id'],
                'unique',
                'targetAttribute' => ['worker_id', 'document_id', 'has_position_id'],
                'message'         => 'The combination of Worker ID, Document ID and Has Position ID has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'worker_id'       => 'Worker ID',
            'document_id'     => 'Document ID',
            'has_position_id' => 'Has Position ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerDriverLicenses()
    {
        return $this->hasOne(WorkerDriverLicense::className(), ['worker_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasPosition()
    {
        return $this->hasOne(WorkerHasPosition::className(), ['id' => 'has_position_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::className(), ['document_id' => 'document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }
}
