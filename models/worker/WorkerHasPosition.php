<?php

namespace app\models\worker;


use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%worker_has_position}}".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property integer $position_id
 * @property integer $active
 * @property integer $position_class_id
 * @property integer $group_id
 * @property double $rating
 *
 * @property WorkerHasCar[] $workerHasCars
 * @property Car[] $cars
 * @property Worker $worker
 */
class WorkerHasPosition extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%worker_has_position}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'position_id'], 'required'],
            [['worker_id', 'position_id', 'active', 'position_class_id', 'group_id'], 'integer'],
            [['rating'], 'number'],
            [
                ['worker_id', 'position_id'],
                'unique',
                'targetAttribute' => ['worker_id', 'position_id'],
                'message'         => 'The combination of Worker ID and Position ID has already been taken.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'worker_id'         => 'Worker ID',
            'position_id'       => 'Position ID',
            'active'            => 'Active',
            'position_class_id' => 'Position Class ID',
            'group_id'          => 'Group ID',
            'rating'            => 'Rating',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerHasCars()
    {
        return $this->hasMany(WorkerHasCar::className(), ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCars()
    {
        return $this->hasMany(Car::className(), ['car_id' => 'car_id'])->viaTable('{{%worker_has_car}}',
            ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorker()
    {
        return $this->hasOne(Worker::className(), ['worker_id' => 'worker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocuments()
    {
        return $this->hasMany(WorkerHasDocument::className(), ['has_position_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkerPositionFieldValues()
    {
        return $this->hasMany(WorkerPositionFieldValue::className(), ['has_position_id' => 'id']);
    }
}
