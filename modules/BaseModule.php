<?php


namespace app\modules;

use app\filters\interfaces\IFilter;
use app\helpers\TenantRequestDataHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Module;


class BaseModule extends Module
{
    /** @var array */
    public $filters = [];

    private $tenantId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->runFilters();
        parent::init();
    }

    /**
     * Get tenant id
     * @return int
     */
    public function getTenantId(): int
    {
        if (empty($this->tenantId)) {
            $this->tenantId = TenantRequestDataHelper::getId();
        }

        return $this->tenantId;
    }


    /**
     * Run filters
     * @throws InvalidConfigException
     */
    private function runFilters()
    {
        if (!empty($this->filters)) {
            foreach ($this->filters as $filter) {
                /** @var IFilter $filter */
                $filter = Yii::createObject($filter);
                if (!$filter instanceof IFilter) {
                    throw new InvalidConfigException('The filter must be implement of IFilter');
                }

                $filter->run();
            }
        }
    }
}