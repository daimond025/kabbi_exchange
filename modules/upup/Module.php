<?php

namespace app\modules\upup;


use app\modules\BaseModule;
use app\modules\upup\components\RequestUrl;
use Yii;

/**
 * upup module definition class
 * @property RequestUrl $requestUrl
 */
class Module extends BaseModule
{

    CONST EXCHANGE_PROGRAM_ID = 1;
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\upup\controllers';
    public $protocolVersion;
    private $settings;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->setSettings();
        Yii::configure($this, require(__DIR__ . '/config.php'));
    }


    private function setSettings()
    {
        $this->settings = [
            'tenantId' => $this->getTenantId()
        ];
    }
}
