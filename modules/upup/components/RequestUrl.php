<?php


namespace app\modules\upup\components;


use yii\base\InvalidConfigException;
use yii\base\BaseObject;

class RequestUrl extends BaseObject
{
    public $baseUrl;
    public $action;
    public $version;
    public $apikey;
    public $clid;
    public $showAuto;
    public $rate;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->baseUrl)) {
            throw new InvalidConfigException('The base url must be set');
        }
    }

    public function getUrl(): string
    {
        $url = $this->baseUrl;
        $url .= $this->action;

        return $url;
    }

    public function getFullUrl(): string
    {
        $url = $this->getUrl();

        $this->addParam($url, 'version');
        $this->addParam($url, 'clid');
        $this->addParam($url, 'apikey');
        return $url;
    }

    private function addParam(string &$url, string $paramName)
    {
        if (!empty($this->$paramName)) {
            if (strpos($url, '?') !== false) {
                $symbol = '&';
            } else {
                $symbol = '?';
            }

            $url .= $symbol . $paramName . '=' . $this->$paramName;
        }
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return RequestUrl
     */
    public function setAction(string $action): self
    {
        $this->action = $action;

        return $this;
    }


    /**
     * @return string
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     * @return RequestUrl
     */
    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }


    /**
     * @return string
     */
    public function getApikey(): ?string
    {
        return $this->apikey;
    }

    /**
     * @param mixed $apiKey
     * @return RequestUrl
     */
    public function setApiKey(string $apiKey): self
    {
        $this->apikey = $apiKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getClid(): ?string
    {
        return $this->clid;
    }

    /**
     * @param mixed $clid
     * @return RequestUrl
     */
    public function setClid(string $clid): self
    {
        $this->clid = $clid;

        return $this;
    }


    /**
     * @return string
     */
    public function getShowAuto(): ?string
    {
        return $this->showAuto;
    }

    /**
     * @param mixed $showAuto
     * @return RequestUrl
     */
    public function setShowAuto(string $showAuto): self
    {
        $this->showAuto = $showAuto;

        return $this;
    }


    /**
     * @return string
     */
    public function getRate(): ?string
    {
        return $this->rate;
    }

    /**
     * @param mixed $rate
     * @return RequestUrl
     */
    public function setRate(string $rate): self
    {
        $this->rate = $rate;

        return $this;
    }
}