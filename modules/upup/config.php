<?php
return [
    'components' => [
        'requestUrl' => [
            'class'   => 'app\modules\upup\components\RequestUrl',
            'baseUrl' => 'http://upup.su/',
        ],
    ],
];