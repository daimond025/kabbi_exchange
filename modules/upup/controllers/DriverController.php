<?php

namespace app\modules\upup\controllers;

use app\controllers\BaseController;
use app\modules\upup\helpers\RequestUrl;
use app\modules\upup\services\DriverService;
use app\repositories\exceptions\NotFoundException;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * @TODO выгрузка водителей - переделать
 * Class DriverController
 * Класс для управления сущностью "Driver"
 * @package app\modules\v1\controllers
 */
class DriverController extends BaseController
{
    use RequestUrl;

    /** @var DriverService */
    private $driverService;

    public function __construct($id, $module, DriverService $driverService, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->driverService = $driverService;
    }

    /**
     * GET
     * Export cabs(drivers + cars) to exchange
     * @return array
     */
    public function actionIndex()
    {
        Yii::$container->set('app\components\xml\builder\XmlBuilder', [
            'rootTag'        => 'Cars',
            'rootAttributes' => [['v', $this->module->protocolVersion]],
        ]);

        return $this->driverService->exportDrivers();
    }

    /**
     * POST
     * Send driver status to exchange
     * @param string $callsign
     * @throws NotFoundHttpException
     */
    public function actionSendStatus($callsign)
    {
        $status = Yii::$app->request->post('status');
        $requestUrl = $this->getRequestUrl('carstatus');

        try {
            $this->driverService->sendStatus($callsign, $status, $requestUrl);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }
    }

    /**
     * Sending coordinates to exchange
     */
    public function actionSendLocations()
    {
        $requestUrl = $this->getRequestUrl('taxicollect');

        $this->driverService->sendLocations($requestUrl);
    }
}
