<?php


namespace app\modules\upup\controllers;


use app\controllers\BaseController;
use app\modules\upup\helpers\RequestUrl;
use app\modules\upup\Module;
use app\modules\upup\services\OrderBuyService;
use Yii;
use yii\web\BadRequestHttpException;

/**
 * @TODO покупка заказа из биржы - переделать
 * For buy order from exchange to gootax
 * Class OrderBuyController
 * @package app\modules\upup\controllers
 */
class OrderBuyController extends BaseController
{
    use RequestUrl;

    /** @var OrderBuyService */
    private $orderBuyService;

    public function init()
    {
        parent::init();

        $this->orderBuyService = Yii::createObject(OrderBuyService::class, [$this->getRequestUrl()]);
    }

    /**
     * POST
     * Предложение заказа
     */
    public function actionOfferOrder()
    {
        /** @var Module $module */
        $module = $this->module;
        $this->orderBuyService->offerOrder(Yii::$app->request->post(), $module->getTenantId(), $module->getCityId());
    }

    /**
     * POST
     * Закрепление заказа
     */
    public function actionSetCar()
    {

    }

    /**
     * POST
     * Сообщение о готовности водителя выполнить заказ
     */
    public function actionConfirmOrder()
    {
        $orderId = Yii::$app->request->post('order_id');

        if (empty($orderId)) {
            throw new BadRequestHttpException('Missing order_id');
        }
    }

    /**
     * POST
     * Обновление статуса заказа
     */
    public function actionChangeStatus()
    {
        $orderId = Yii::$app->request->post('order_id');

        if (empty($orderId)) {
            throw new BadRequestHttpException('Missing order_id');
        }

        $status = Yii::$app->request->post('status');

        if (empty($status)) {
            throw new BadRequestHttpException('Missing a order status');
        }
    }

    /**
     * GET
     * Отмена заказа
     * @param string $orderid
     * @param string $reason
     */
    public function actionReject($orderid, $reason)
    {

    }
}