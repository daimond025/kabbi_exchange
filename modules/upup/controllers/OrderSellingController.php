<?php


namespace app\modules\upup\controllers;


use app\controllers\BaseController;
use app\exceptions\BadRequestToService;
use app\modules\upup\events\order\listeners\OrderDeleteListener;
use app\modules\upup\events\order\listeners\OrderSoldListener;
use app\modules\upup\events\order\listeners\OrderUpdateListener;
use app\modules\upup\exceptions\UpdateOrderException;
use app\modules\upup\helpers\RequestUrl;
use app\modules\upup\Module;
use app\modules\upup\services\OrderSellingService;
use app\repositories\exceptions\NotFoundException;
use app\repositories\exceptions\WrongDataException;
use app\modules\upup\exceptions\AddOrderException;
use Yii;
use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\httpclient\Exception;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * For selling gootax order to exchange
 * Class OrderSellingController
 * @package app\modules\upup\controllers
 */
class OrderSellingController extends BaseController
{
    use RequestUrl;

    /** @var OrderSellingService */
    private $orderSellingService;

    public function init()
    {
        parent::init();
        try {
            $clid = Yii::$app->request->post('clid');
            $apiKey = Yii::$app->request->post('api_key');
            $showAuto = Yii::$app->request->post('show_auto');
            $rate = Yii::$app->request->post('rate');
            $this->orderSellingService = Yii::createObject(OrderSellingService::class, [
                $this->getRequestUrl(
                    [
                        'action'   => 'api',
                        'clid'     => $clid,
                        'apiKey'   => $apiKey,
                        'showAuto' => $showAuto,
                        'rate'     => $rate,
                    ])]);
        } catch (InvalidConfigException $ex) {

        }

    }


    /**
     * Add order to Up&Up exchange
     * @return array
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionAdd(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $orderId = Yii::$app->request->post('order_id');
        if (empty($orderId)) {
            throw new BadRequestHttpException('Missing order_id');
        }
        try {
            $orderSoldListener = Yii::createObject(OrderSoldListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::ADD_EVENT, [$orderSoldListener, 'handle']);
            $upUpOrderId = $this->orderSellingService->add($orderId);
            return [
                'exchange_order_id' => $upUpOrderId
            ];
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), 404);
        } catch (WrongDataException | InvalidConfigException $e) {
            throw new BadRequestHttpException($e->getMessage(), 500);
        } catch (AddOrderException $ex) {
            throw new HttpException(400, $ex->getMessage());
        } catch (BadRequestToService | Exception $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }

    /**
     * Update order at exchange
     * @param int $order_id
     * @return array|null
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionUpdate(int $order_id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            $orderUpdateListener = Yii::createObject(OrderUpdateListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::UPDATE_EVENT, [$orderUpdateListener, 'handle']);
            $upUpOrderId = $this->orderSellingService->update($order_id);
            return [
                'exchange_order_id' => $upUpOrderId
            ];
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), 404);
        } catch (WrongDataException | InvalidConfigException $e) {
            throw new BadRequestHttpException($e->getMessage(), 500);
        } catch (UpdateOrderException $ex) {
            throw new HttpException(400, $ex->getMessage());
        } catch (BadRequestToService | Exception $e) {
            throw new HttpException(503, $e->getMessage());
        }

    }


    /**
     * Delete order form exchange
     * @param int $order_id
     * @throws BadRequestHttpException
     * @throws HttpException
     * @throws NotFoundHttpException
     */
    public function actionDelete(int $order_id): void
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            $orderDeleteListener = Yii::createObject(OrderDeleteListener::class);
            Event::on(OrderSellingService::class, OrderSellingService::DELETE_EVENT,
                [$orderDeleteListener, 'handle']);
            $this->orderSellingService->delete($order_id);
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage(), 404);
        } catch (WrongDataException | InvalidConfigException $e) {
            throw new BadRequestHttpException($e->getMessage(), 500);
        } catch (AddOrderException $ex) {
            throw new HttpException(400, $ex->getMessage());
        } catch (BadRequestToService | Exception $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }


    /**
     * Get info about order at exchange
     * @param int $order_id
     * @param int $check
     * @return array|mixed
     * @throws HttpException
     */
    public function actionInfo(int $order_id, int $check = 0)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        try {
            return $this->orderSellingService->getInfo($order_id, $check);
        } catch (BadRequestToService | Exception $e) {
            throw new HttpException(503, $e->getMessage());
        }
    }

    /**
     * Exchange call this action to change order status at Gootax
     * @param string $orderid
     * @param string $status
     * @throws NotFoundHttpException
     */
    public function actionChangeStatus($orderid, $status): void
    {
        /** @var Module $module */
        $module = $this->module;
        try {
            $this->orderSellingService->changeStatus($orderid, $status, $module->getTenantId());
        } catch (NotFoundException $e) {
            throw new NotFoundHttpException($e->getMessage());
        }
    }
}