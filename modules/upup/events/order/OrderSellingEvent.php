<?php


namespace app\modules\upup\events\order;


use yii\base\Event;

class OrderSellingEvent extends Event
{
    public $upupOrderId;
    public $orderId;
}