<?php


namespace app\modules\upup\helpers;


class DriverStatusHelper
{
    public static function getExchangeStatus(string $gootaxDriverStatus): string
    {
        return $gootaxDriverStatus === GootaxDriverStatus::FREE ?
            ExchangeDriverStatus::FREE : ExchangeDriverStatus::BUSY;
    }
}