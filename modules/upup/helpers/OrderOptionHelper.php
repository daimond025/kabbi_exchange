<?php


namespace app\modules\upup\helpers;


class OrderOptionHelper
{
    public static function getOptionMap(): array
    {
        return [
            'animal'      => 8,
            'card'        => 6,
            'children'    => 4,
            'conditioner' => 1,
            'nosmoking'   => 15,
        ];
    }

    /**
     * @param string $optionName Exchange option name
     * @param array $carOptions Gootax car`s options
     * @return int
     */
    public static function hasOption($optionName, $carOptions): int
    {
        $optionMap = self::getOptionMap();

        return (int)in_array($optionMap[$optionName], $carOptions);
    }

    public static function getOptionKeys(): array
    {
        return array_keys(self::getOptionMap());
    }
}