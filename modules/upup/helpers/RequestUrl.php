<?php


namespace app\modules\upup\helpers;


use app\modules\upup\components\RequestUrl as Url;
use app\modules\upup\Module;
use yii\rest\Controller;

/**
 * Class RequestUrl
 * @package app\modules\upup\helpers
 * @mixin Controller
 */
trait RequestUrl
{

    public $params = [
        'action',
        'clid',
        'apiKey',
        'showAuto',
        'rate'
    ];

    /**
     * @param $params
     * @return Url
     */
    public function getRequestUrl($params): Url
    {
        /** @var Module $module */
        $module = $this->module;
        $requestUrl = $module->requestUrl
            ->setVersion($module->protocolVersion);

        if (isset($params['action'])) {
            $requestUrl->setAction($params['action']);
        }
        if (isset($params['clid'])) {
            $requestUrl->setClid($params['clid']);
        }
        if (isset($params['apiKey'])) {
            $requestUrl->setApiKey($params['apiKey']);
        }
        if (isset($params['showAuto'])) {
            $requestUrl->setShowAuto($params['showAuto']);
        }
        if (isset($params['rate'])) {
            $requestUrl->setRate($params['rate']);
        }
        return $requestUrl;
    }
}