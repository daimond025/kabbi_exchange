<?php


namespace app\modules\upup\models\clients;


use app\models\client\Client as ClientRecord;
use app\modules\upup\repositories\ClientRepository;

class Client
{
    const CLIENT_NAME = 'Upup client';

    /** @var  ClientRepository */
    private $clientRepository;
    private $tenantId;
    private $cityId;

    public function __construct(int $tenantId, int $cityId, ClientRepository $clientRepository)
    {
        $this->tenantId = $tenantId;
        $this->cityId = $cityId;
        $this->clientRepository = $clientRepository;
    }

    public function getClientId(): int
    {
        $clientId = $this->clientRepository->getClientId($this->tenantId, $this->cityId, self::CLIENT_NAME);

        if (empty($clientId)) {
            $clientId = $this->createClient();
        }

        return $clientId;
    }

    private function createClient(): int
    {
        $client = new ClientRecord([
            'tenant_id' => $this->tenantId,
            'city_id'   => $this->cityId,
            'name'      => self::CLIENT_NAME,
        ]);

        $this->clientRepository->save($client);

        return $client->client_id;
    }
}