<?php


namespace app\modules\upup\models\drivers;


use RuntimeException;

class Cab
{
    private $driver;
    private $car;
    private $geo;

    public function __construct(array $cabData)
    {
        if (!isset($cabData['worker'], $cabData['car'])) {
            throw new RuntimeException('Active driver data must containts of worker, car info');
        }

        $this->driver = $cabData['worker'];
        $this->car = $cabData['car'];
        $this->geo = $cabData['geo'];
    }

    /**
     * @return array
     */
    public function getDriver(): array
    {
        return $this->driver;
    }

    /**
     * @param array $driver
     */
    public function setDriver(array $driver)
    {
        $this->driver = $driver;
    }

    /**
     * @return array
     */
    public function getCar(): array
    {
        return $this->car;
    }

    /**
     * @param array $car
     */
    public function setCar(array $car)
    {
        $this->car = $car;
    }

    /**
     * @return string
     * @throws RuntimeException
     */
    public function getId(): string
    {
        if (!isset($this->car['car_id'], $this->driver['worker_id'])) {
            throw new RuntimeException('Missing id for worker or car');
        }

        return "w{$this->driver['worker_id']}c{$this->car['car_id']}";
    }

    /**
     * @return mixed
     */
    public function getGeo(): array
    {
        return $this->geo;
    }
}