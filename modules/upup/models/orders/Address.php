<?php


namespace app\modules\upup\models\orders;


class Address
{
    /**
     * @var array
     */
    private $addressDelivery;

    /**
     * @var array
     */
    private $addressDestination;

    public function __construct(string $gootaxAddress)
    {
        $this->setDeliveryAddress($gootaxAddress);
        $this->setDestinationAddress($gootaxAddress);
    }

    public function getDeliveryCity(): ?string
    {

        return $this->getDestinationAddressPart('city');
    }

    public function getDestinationCity(): ?string
    {
        return $this->getDestinationAddressPart('city');
    }


    public function getDeliveryStreet(): string
    {
        if (!isset($this->addressDelivery['street'])) {
            throw new \DomainException('The delivery street of order is empty');
        }

        return $this->addressDelivery['street'];
    }

    public function getDestinationStreet(): ?string
    {
        return $this->getDestinationAddressPart('street');
    }

    public function getDeliveryBuilding(): ?string
    {
        return $this->getDeliveryAddressPart('house');
    }

    public function getDestinationBuilding(): ?string
    {
        return $this->getDestinationAddressPart('house');
    }

    public function getDeliveryHousing(): ?string
    {
        return $this->getDeliveryAddressPart('housing');
    }

    public function getDestinationHousing(): ?string
    {
        return $this->getDestinationAddressPart('housing');
    }

    public function getDeliveryPorch(): ?string
    {
        return $this->getDeliveryAddressPart('porch');
    }

    public function getDestinationPorch(): ?string
    {
        return $this->getDestinationAddressPart('porch');
    }


    public function getDeliveryLat(): ?string
    {
        return $this->getDeliveryAddressPart('lat');
    }

    public function getDestinationLat(): ?string
    {
        return $this->getDestinationAddressPart('lat');
    }

    public function getDeliveryLon(): ?string
    {
        return $this->getDeliveryAddressPart('lon');
    }

    public function getDestinationLon(): ?string
    {
        return $this->getDestinationAddressPart('lon');
    }


    private function getDeliveryAddressPart(string $addressPart): ?string
    {
        return $this->addressDelivery[$addressPart] ?? null;
    }

    private function getDestinationAddressPart(string $addressPart): ?string
    {
        return $this->addressDestination[$addressPart] ?? null;
    }

    private function setDeliveryAddress(string $gootaxAddress)
    {
        $address = unserialize($gootaxAddress);

        $this->addressDelivery = current($address);
    }

    private function setDestinationAddress(string $gootaxAddress)
    {
        $address = unserialize($gootaxAddress);
        if (count($address) > 1) {
            $this->addressDestination = end($address);
        } else {
            $this->addressDestination = null;
        }

    }
}