<?php


namespace app\modules\upup\models\orders\providers;


use app\modules\upup\models\clients\Client;
use app\modules\upup\models\interfaces\IDataProvider;
use app\modules\upup\models\orders\AddressConverter;
use app\modules\upup\models\tariff\TariffGootaxConverter;
use Yii;

/**
 * Class OrderBuyDataProvider
 * Getting adapted data from exchange for gootax
 * @package app\modules\upup\models\orders\providers
 */
class OrderBuyDataProvider implements IDataProvider
{
    /**
     * @var array
     */
    private $exchangeOrderData;
    /**
     * @var int
     */
    private $tenantId;
    /**
     * @var int
     */
    private $cityId;

    public function __construct(array $exchangeOrderData, int $tenantId, int $cityId)
    {
        $this->exchangeOrderData = $exchangeOrderData;
        $this->tenantId = $tenantId;
        $this->cityId = $cityId;
    }

    public function getData(): array
    {
        return [
            'order'  => [
                'tenant_id' => $this->tenantId,
                'city_id'   => $this->cityId,
                'address'   => $this->getAddress(),
                'comment'   => $this->getComment(),
                'client_id' => $this->getClientId(),
            ],
            'tariff' => $this->getTariffData(),
        ];
    }

    private function getAddress(): string
    {
        $destinations = $this->exchangeOrderData['Destinations']['Destination'] ?? [];

        return (new AddressConverter($this->exchangeOrderData['Source'], $destinations, $this->cityId))->getData();
    }

    /**
     * @return string|null
     */
    private function getComment(): ?string
    {
        return $this->exchangeOrderData['Comments'] ?? null;
    }

    private function getClientId(): int
    {
        /** @var Client $clientService */
        $clientService = Yii::createObject(Client::class, [$this->tenantId, $this->cityId]);

        return $clientService->getClientId();
    }

    private function getTariffData(): array
    {
        return (new TariffGootaxConverter($this->exchangeOrderData['Tariffs']['Tariff']))->getTariff();
    }
}