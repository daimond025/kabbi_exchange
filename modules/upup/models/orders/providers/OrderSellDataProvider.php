<?php


namespace app\modules\upup\models\orders\providers;


use app\modules\upup\helpers\OrderOptionHelper;
use app\modules\upup\models\interfaces\IDataProvider;
use app\modules\upup\models\orders\Address;
use app\modules\upup\models\orders\Order;
use app\modules\upup\models\tariff\TariffExchangeConverter;
use app\modules\upup\repositories\TariffRepository;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class OrderSellDataProvider
 * Getting data for selling order to exchange
 * @package app\modules\upup\models\orders\providers
 */
class OrderSellDataProvider implements IDataProvider
{
    /**
     * @var Order
     */
    private $order;
    private $options = null;

    CONST TIME_OFFSET = 10;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get data
     * @return array
     * @throws \yii\base\InvalidConfigException
     */
    public function getData(): array
    {
        $address = new Address($this->order->address);

        return [
            'deadline'    => $this->getDeadline(self::TIME_OFFSET),
            'number'      => $this->order->client->phone->value,
            'name'        => $this->order->client->getFullName(),
            'city'        => $this->order->city->name ? $this->order->city->name : $address->getDeliveryCity(),
            'dcity'       => '',
            'street'      => $address->getDeliveryStreet(),
            'dstreet'     => $address->getDestinationStreet(),
            'home'        => $address->getDeliveryBuilding(),
            'dhome'       => $address->getDestinationBuilding(),
            'structure'   => $address->getDeliveryHousing(),
            'dstructure'  => $address->getDestinationHousing(),
            'porch'       => $address->getDeliveryPorch(),
            'dporch'      => $address->getDestinationPorch(),
            'tarif'       => $this->getTariff(),
            'comment'     => $this->order->comment,
            'card'        => OrderOptionHelper::hasOption('card', $this->getOptions()),
            'children'    => OrderOptionHelper::hasOption('children', $this->getOptions()),
            'nosmoking'   => OrderOptionHelper::hasOption('nosmoking', $this->getOptions()),
            'animal'      => OrderOptionHelper::hasOption('animal', $this->getOptions()),
            'conditioner' => OrderOptionHelper::hasOption('conditioner', $this->getOptions()),
            'external_id' => $this->order->order_id,
            'lat'         => $address->getDeliveryLat(),
            'dlat'        => $address->getDestinationLat(),
            'lon'         => $address->getDeliveryLon(),
            'dlon'        => $address->getDestinationLon(),
        ];
    }


    /**
     * Getting order time by unix timestamp without city offset
     * @param int $timeOffset add minutes if now time more than order time
     * @return int
     */
    private function getDeadline(int $timeOffset = 0): int
    {
        $nowTime = time();
        $orderTime = $this->order->order_time - $this->order->time_offset;
        //Add time offset to order
        if (($nowTime > $orderTime) || ($orderTime > $nowTime && $orderTime - $nowTime < $timeOffset * 60)) {
            $orderTime = $orderTime + $timeOffset * 60;
        }
        return $orderTime;
    }

    private function getOptions(): array
    {
        if (is_null($this->options)) {
            $this->options = ArrayHelper::getColumn($this->order->options, 'option_id', false);
        }

        return $this->options;
    }

    /**
     * Get tariff
     * @return string
     * @throws \yii\base\InvalidConfigException
     */
    private function getTariff(): string
    {
        /** @var TariffRepository $tariffRepository */
        $tariffRepository = Yii::createObject(TariffRepository::class, [$this->order->tenant_id]);

        $tariffConverter = new TariffExchangeConverter($this->order->order_id, $tariffRepository);

        return $tariffConverter->getTariff();
    }
}