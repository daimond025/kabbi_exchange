<?php


namespace app\modules\upup\models\tariff;


use yii\base\NotSupportedException;

class CarClassMapper
{
    /**
     * Из доков UpUp:
     * Название класса при создании заказа через API Описание класса Аббревиатура класса в тарифе
     * comfort                                       комфорт         c
     * business                                      бизнес          b
     * universal                                     универсал       u
     * econom                                        эконом          e
     * minivan                                       минивэн         v
     * minibus                                       микроавтобус    a
     * @param int $carClassId
     * @return string
     * @throws NotSupportedException
     */
    public static function getCarClass(int $carClassId): string
    {
        switch ($carClassId) {
            case 1:
                return 'e';
            case 2:
                return 'c';
            case 3:
                return 'b';
            case 5:
                return 'v';
            case 6:
                return 'u';
            case 7:
                return 'a';
            default:
                throw new NotSupportedException('Not supported the car class for upup exchange');
        }
    }
}