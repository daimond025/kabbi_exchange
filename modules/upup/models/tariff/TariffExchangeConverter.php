<?php


namespace app\modules\upup\models\tariff;


use app\modules\upup\repositories\TariffRepository;

class TariffExchangeConverter
{
    const FIX_TARIFF      = 'f';
    const TIME_TARIFF     = 'm';
    const DISTANCE_TARIFF = 'k';
    /**
     * @var TariffRepository
     */
    private $repository;
    /** @var array All data */
    private $data;
    /** @var array Tariff data based on location */
    private $tariffData;

    public function __construct(int $orderId, TariffRepository $repository)
    {
        $this->repository = $repository;
        $this->data = $this->repository->getTariff($orderId);
        $this->setTariffData();
    }

    /**
     * Из доков UpUp:
     * Название тарифа формируется следующим образом:
     * “c/m/p1/p2/p3/p4/p5/p6”, где
     * c – класс машины
     * m – тарификация (m - по минутам, k –по километрам).
     * p1 – минимальная стоимость заказа (в целых рублях)
     * p2 – количество километров (минут), включенных в заказ
     * p3 – стоимость в целых рублях дополнительного километра (минуты)
     * p4 – бесплатное ожидание (минуты)
     * p5 – платное ожидание (рубли в минуту)
     * p6 – стоимость километра за городом в рублях
     * p7 – количество минут вынужденного простоя, включенных в заказ
     * p8 – стоимость дополнительной минуты вынужденного простоя
     * p9 – скорость, ниже которой, включается вынужденный простой (исполнитель может использовать
     * собственное значение, не превышающее 25 км\час).
     * Если для заказа установлена фиксированная стоимость поездки, то он будет передаваться в виде:
     * “c/f/p”, где
     * c – класс машины
     * p – стоимость поездки (в целых рублях).
     * @return string
     */
    public function getTariff(): string
    {
        return implode('/', $this->getTariffParams());
    }

    private function getTariffParams(): array
    {
        $tariffing = $this->getTariffing();

        $tariffParams = [
            $this->getClass(),
            $tariffing,
        ];

        if ($tariffing === self::FIX_TARIFF) {
            $tariffParams = array_merge($tariffParams, [
                $this->getSummaryCost(),
            ]);
        } else {
            $tariffParams = array_merge($tariffParams, [
                $this->getMinPrice(),//минимальная цена заказа в рублях
                $this->getPlantingInclude(),//кол-во километров/минут вкл в заказ
                $this->getNextPrice(),//стоимость в целых рублях дополнительного километра (минуты)
                $this->getFreeWaitTime(),//бесплатное ожидание (минуты)
                $this->getWaitPrice(),// платное ожидание (рубли в минуту)
                $this->getCountrysidePrice(),// стоимость километра за городом в рублях
                $this->getWaitDriveTime(), //количество минут вынужденного простоя, включенных в заказ
                $this->getAdditionalCostOfForcedDowntime(),//– стоимость дополнительной минуты вынужденного простоя
                $this->getSpeedPaidWaiting(),//скорость, ниже которой, включается вынужденный простой
            ]);
        }

        return $tariffParams;
    }

    private function getClass(): string
    {
        return CarClassMapper::getCarClass($this->data['tariff']['class_id']);
    }

    private function getTariffing(): string
    {
        if ($this->isFix()) {
            return self::FIX_TARIFF;
        }
        $accrual = $this->getAccrual();
        switch ($accrual) {
            case 'FIX':
                return self::FIX_TARIFF;
            case 'TIME':
                return self::TIME_TARIFF;
            default:
                return self::DISTANCE_TARIFF;
        }
    }

    private function isCity(): bool
    {
        return $this->data['costData']['start_point_location'] === 'in';
    }

    private function isDay(): bool
    {
        return (int)$this->data['costData']['tariffInfo']['isDay'] === 1;
    }


    private function setTariffData()
    {
        $tariffDataLocationKey = $this->isCity() ? 'tariffDataCity' : 'tariffDataTrack';
        $this->tariffData = $this->data['costData']['tariffInfo'][$tariffDataLocationKey];
    }

    private function getAccrual(): string
    {
        return $this->tariffData['accrual'];
    }

    private function isFix(): bool
    {
        return (int)$this->data['costData']['is_fix'] === 1;
    }


    private function getSummaryCost()
    {
        return $this->data['costData']['summary_cost'];
    }

    private function getMinPrice(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['min_price_day'];
        }

        return $this->isDay() ? $this->tariffData['min_price_day'] : $this->tariffData['min_price_night'];
    }

    private function getPlantingInclude(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['planting_include_day'];
        }

        return $this->isDay() ? $this->tariffData['planting_include_day'] : $this->tariffData['planting_include_night'];
    }

    private function getNextPrice(): string
    {
        if ($this->getAccrual() === 'INTERVAL') {
            if (!$this->isAllowDayNignt()) {
                $intervalData = $this->tariffData['next_km_price_day'];
            } else {
                $intervalData = $this->isDay() ? $this->tariffData['next_km_price_day'] : $this->tariffData['next_km_price_night'];
            }

            $intervalData = unserialize($intervalData);

            return $intervalData[0]['price'];
        }

        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['next_km_price_day'];
        }

        return $this->isDay() ? $this->tariffData['next_km_price_day'] : $this->tariffData['next_km_price_night'];
    }

    private function getCountrysidePrice(): string
    {
        $tariffData = $this->data['costData']['tariffInfo']['tariffDataTrack'];

        if (!$this->isAllowDayNignt($tariffData)) {
            return $tariffData['next_km_price_day'];
        }

        return $this->isDay() ? $tariffData['next_km_price_day'] : $tariffData['next_km_price_night'];
    }

    private function isAllowDayNignt(?array $tariffData = null): bool
    {
        $data = $tariffData ?? $this->tariffData;

        return (int)$data['allow_day_night'] === 1;
    }

    private function getWaitDriveTime(): string
    {

        if (!$this->isAllowDayNignt()) {
            return (int)($this->tariffData['wait_driving_time_day'] / 60);
        }

        return $this->isDay() ? (int)($this->tariffData['wait_driving_time_day'] / 60) : (int)($this->tariffData['wait_driving_time_night'] / 60);
    }

    private function getFreeWaitTime(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['wait_time_day'];
        }

        return $this->isDay() ? $this->tariffData['wait_time_day'] : $this->tariffData['wait_time_night'];
    }

    private function getWaitPrice(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['wait_price_day'];
        }

        return $this->isDay() ? $this->tariffData['wait_price_day'] : $this->tariffData['wait_price_night'];
    }

    private function getSpeedPaidWaiting(): string
    {
        if (!$this->isAllowDayNignt()) {
            return $this->tariffData['speed_downtime_day'];
        }

        return $this->isDay() ? $this->tariffData['speed_downtime_day'] : $this->tariffData['speed_downtime_night'];
    }

    private function getAdditionalCostOfForcedDowntime(): string
    {
        return $this->getWaitPrice();
    }
}