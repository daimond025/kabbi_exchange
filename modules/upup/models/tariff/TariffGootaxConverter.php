<?php


namespace app\modules\upup\models\tariff;


class TariffGootaxConverter
{
    /**
     * @var string
     */
    private $exchangeTariff;

    public function __construct(string $exchangeTariff)
    {
        $this->exchangeTariff = $exchangeTariff;
    }

    public function getTariff(): array
    {
        $tariffParts = $this->getParts();

        return [
            //todo Реализовать формат данных тарифа гутакса
        ];
    }

    private function getParts(): array
    {
        return explode('/', $this->exchangeTariff);
    }
}