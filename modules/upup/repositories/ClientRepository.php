<?php


namespace app\modules\upup\repositories;


use app\models\client\Client;
use RuntimeException;

class ClientRepository
{
    /**
     * @param int $tenantId
     * @param int $cityId
     * @param string $name
     * @return false|null|string
     */
    public function getClientId(int $tenantId, int $cityId, string $name)
    {
        return Client::find()
            ->where([
                'tenant_id' => $tenantId,
                'city_id'   => $cityId,
                'name'      => $name,
            ])
            ->select('client_id')
            ->scalar();
    }

    /**
     * @param Client $model
     * @throws RuntimeException
     * @return bool
     */
    public function save(Client $model)
    {
        if ($model->save() === false) {
            throw new RuntimeException('Error saving the client.');
        }

        return true;
    }
}