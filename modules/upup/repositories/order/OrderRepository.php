<?php


namespace app\modules\upup\repositories\order;


use app\modules\upup\models\orders\Order;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class OrderRepository
{
    /**
     * @param int $orderId
     * @return Order
     * @throws NotFoundHttpException
     */
    public function getById(int $orderId): Order
    {
        /** @var Order $order */
        $order = Order::find()
            ->where(['order_id' => $orderId])
            ->with([
                'client' => function (ActiveQuery $query) {
                    $query->select(['client_id', 'last_name', 'name', 'second_name']);
                    $query->with('phone');
                },
                'city'   => function (ActiveQuery $query) {
                    $query->select(['city_id', 'name']);
                },
                'options',
            ])
            ->limit(1)
            ->one();

        if (empty($order)) {
            throw new NotFoundHttpException('The order was not found');
        }

        return $order;
    }

    public function isExist(int $orderId): bool
    {
        return Order::find()->where(['order_id' => $orderId])->exists();
    }
}