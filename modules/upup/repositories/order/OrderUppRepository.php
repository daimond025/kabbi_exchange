<?php


namespace app\modules\upup\repositories\order;


use app\modules\upup\models\orders\OrderUpup;
use app\repositories\exceptions\NotFoundException;
use RuntimeException;
use Yii;

class OrderUppRepository
{
    /**
     * Get by gootax order id
     * @param int $orderId
     * @return OrderUpup
     * @throws NotFoundException
     */
    public function getById(int $orderId): OrderUpup
    {
        $res = OrderUpup::findOne(['order_id' => $orderId]);

        if (empty($res)) {
            throw new NotFoundException('The upup order has not been found');
        }

        return $res;
    }

    /**
     * Get by exchange order id
     * @param int $orderId
     * @return OrderUpup
     * @throws NotFoundException
     */
    public function getByUpUpId(int $orderId): OrderUpup
    {
        $res = OrderUpup::findOne(['upup_id' => $orderId]);

        if (empty($res)) {
            throw new NotFoundException('The upup order has not been found');
        }

        return $res;
    }

    /**
     * @param OrderUpup $orderUpup
     * @throws RuntimeException
     */
    public function save(OrderUpup $orderUpup): void
    {
        if ($orderUpup->save(false) === false) {
            throw new RuntimeException('Error saving of upup model');
        }
    }


    /**
     * @param int $orderId
     * @return int
     * @throws \yii\db\Exception
     */
    public function delete(int $orderId): int
    {
        return Yii::$app->db->createCommand()->delete(OrderUpup::tableName(), ['order_id' => $orderId])->execute();
    }
}