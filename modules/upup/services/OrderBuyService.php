<?php


namespace app\modules\upup\services;


use app\modules\upup\components\RequestUrl;
use app\modules\upup\models\orders\providers\OrderBuyDataProvider;

class OrderBuyService extends ServiceBase
{
    /**
     * @var RequestUrl
     */
    private $requestUrl;

    public function __construct(RequestUrl $requestUrl)
    {
        $this->requestUrl = $requestUrl;
    }

    public function offerOrder(array $offerData, int $tenantId, int $cityId)
    {
        $orderData = new OrderBuyDataProvider($offerData, $tenantId, $cityId);
        //todo Реализация предложения заказа
    }
}