<?php


namespace app\modules\upup\services;

use app\components\orderEventQueue\events\OrderUpdateEvent;
use app\components\orderEventQueue\OrderEventSender;
use app\exceptions\BadRequestToService;
use app\modules\upup\components\RequestUrl;
use app\modules\upup\events\interfaces\ISellingEvent;
use app\modules\upup\events\order\OrderSellingEvent;
use app\modules\upup\exceptions\AddOrderException;
use app\modules\upup\exceptions\DeleteOrderException;
use app\modules\upup\exceptions\InfoOrderException;
use app\modules\upup\helpers\OrderStatusHelper;
use app\modules\upup\models\orders\providers\OrderSellDataProvider;
use app\modules\upup\Module;
use app\modules\upup\repositories\order\OrderRepository;
use app\modules\upup\repositories\order\OrderUppRepository;
use app\modules\upup\exceptions\UpdateOrderException;
use app\repositories\exceptions\NotFoundException;
use Predis\NotSupportedException;
use Yii;
use yii\base\Event;
use yii\httpclient\Response;

class OrderSellingService extends ServiceBase implements ISellingEvent
{
    public const ADD_ACTION    = 'add';
    public const UPDATE_ACTION = 'edit';
    public const DELETE_ACTION = 'del';
    public const INFO_ACTION   = 'info';

    /**
     * @var RequestUrl
     */
    private $requestUrl;
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var OrderUppRepository
     */
    private $upupRepository;

    public function __construct(
        RequestUrl $requestUrl,
        OrderRepository $orderRepository,
        OrderUppRepository $upupRepository
    )
    {
        $this->requestUrl = $requestUrl;
        $this->orderRepository = $orderRepository;
        $this->upupRepository = $upupRepository;
    }


    /**
     * Add order to Up&Up exchange
     * @param int $orderId
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestToService
     * @throws AddOrderException
     */
    public function add(int $orderId): string
    {
        $order = $this->orderRepository->getById($orderId);
        $data = array_merge($this->getCommonParams(self::ADD_ACTION), (new OrderSellDataProvider($order))->getData());
        $response = $this->createRequest($data);
        if (!$response->isOk) {
            $this->sendErrorMessage('Error to add the order to UpUp exchange', $response->toString(), self::ADD_ACTION, $data);
            throw new BadRequestToService('The UpUp service has returned the http status code: ' . $response->getStatusCode());
        }
        $responseData = $response->getData();
        if ($responseData['result'] === 'done') {
            Event::trigger($this, self::ADD_EVENT, (new OrderSellingEvent([
                'upupOrderId' => $responseData['id'],
                'orderId'     => $orderId,
            ])));
            return (string)$responseData['id'];
        }
        $this->sendErrorMessage('Error to add the order to UpUp exchange: ', $responseData['error'], self::ADD_ACTION, $data);
        throw new AddOrderException('Error to add the order to UpUp exchange: ' . $responseData['error']);
    }


    /**
     * Update order at exchange
     * @param int $orderId
     * @return string
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     * @throws \yii\web\NotFoundHttpException
     * @throws BadRequestToService
     * @throws UpdateOrderException
     */
    public function update(int $orderId): string
    {

        $order = $this->orderRepository->getById($orderId);
        $upupOrder = $this->upupRepository->getById($orderId);
        $data = array_merge($this->getCommonParams(self::UPDATE_ACTION), (new OrderSellDataProvider($order))->getData(),
            ['id' => $upupOrder->upup_id]);
        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error to edit the order in the exchange', $response->toString(),
                self::UPDATE_ACTION, $data);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        $responseData = $response->getData();
        if ($responseData['result'] === 'done') {
            Event::trigger($this, self::UPDATE_EVENT, (new OrderSellingEvent([
                'upupOrderId' => $responseData['id'],
                'orderId'     => $orderId,
            ])));
            return (string)$responseData['id'];
        }
        $this->sendErrorMessage('Error of updating order at UpUp exchange: ', $responseData['error'],
            self::UPDATE_ACTION, $data);
        throw new UpdateOrderException('Error of updating order at UpUp exchange: ' . $responseData['error']);

    }

    /**
     * Delete order from exchange
     * @param int $orderId
     * @throws \yii\httpclient\Exception
     * @throws BadRequestToService
     * @throws DeleteOrderException
     * @throws NotFoundException
     */
    public function delete(int $orderId)
    {
        try {
            $upupOrder = $this->upupRepository->getById($orderId);
        } catch (NotFoundException $exception) {
            throw new NotFoundException('The upupOrder is not found');
        }

        $data = array_merge($this->getCommonParams(self::DELETE_ACTION), ['id' => $upupOrder->upup_id]);
        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error to delete the order in the exchange', $response->toString(),
                self::DELETE_ACTION, $data);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }
        $responseData = $response->getData();

        if ($responseData['result'] === 'done') {
            Event::trigger($this, self::DELETE_EVENT, (new OrderSellingEvent([
                'orderId' => $orderId,
            ])));
            return;
        }
        $this->sendErrorMessage('Error to delete order from UpUp exchange: ', $responseData['error'],
            self::DELETE_ACTION, $data);
        throw new DeleteOrderException('Error to delete order from UpUp exchange: ' . $responseData['error']);


    }

    /**
     * Get info about order at exchange
     * @param int $orderId
     * @param int $check
     * @return array|mixed
     * @throws \yii\httpclient\Exception
     * @throws BadRequestToService
     * @throws InfoOrderException
     * @throws NotFoundException
     */
    public function getInfo(int $orderId, int $check = 0)
    {
        try {
            $upupOrder = $this->upupRepository->getById($orderId);
        } catch (NotFoundException $exception) {
            throw new NotFoundException('The upupOrder is not found');
        }

        $data = array_merge($this->getCommonParams(self::INFO_ACTION), [
            'id'          => $upupOrder->upup_id,
            'external_id' => $orderId,
            'check'       => $check,
        ]);

        $response = $this->createRequest($data);

        if (!$response->isOk) {
            $this->sendErrorMessage('Error of getting info for the order with id: ' . $orderId, $response->toString(),
                self::INFO_ACTION, $data);

            throw new BadRequestToService('The Upup service has returned the http status code: ' . $response->getStatusCode());
        }

        $responseData = $response->getData();
        if ($responseData['result'] === 'done') {
            return $responseData;
        }
        $this->sendErrorMessage('Error to get order info from UpUp exchange: ', $responseData['error'],
            self::INFO_ACTION, $data);
        throw new InfoOrderException('Error to get order info from UpUp exchange:' . $responseData['error']);
    }

    /**
     * Change order status at Gootax
     * @param string $orderId - order id at exchange
     * @param string $status
     * @param int $tenantId
     * @throws NotFoundException
     */
    public function changeStatus(string $orderId, string $status, int $tenantId): void
    {
        try {
            $upupOrder = $this->upupRepository->getByUpUpId($orderId);
        } catch (NotFoundException $exception) {
            throw new NotFoundException('The upupOrder is not found');
        }
        $gootaxOrderId = $upupOrder->order_id;

        if (!$this->orderRepository->isExist($gootaxOrderId)) {
            throw new NotFoundException('The order is not found');
        }

        $statusId = OrderStatusHelper::getStatus($status);

        if ($statusId === null) {
            Yii::error('Not supported status:' . $status);
            return;
        }
        $params = [
            'status_id' => $statusId,
        ];
        /** @var $orderEventSender OrderEventSender */
        $orderEventSender = Yii::$app->get('orderEventSender');
        /** @var Module $module */

        $event = new OrderUpdateEvent($tenantId, $gootaxOrderId, Module::EXCHANGE_PROGRAM_ID, $params);

        $orderEventSender->send($event);
    }

    /**
     * Create exchange request
     * @param array $data
     * @return Response
     */
    private function createRequest(array $data): Response
    {
        $response = $this->getHttpClient()
            ->createRequest()
            ->setMethod('post')
            ->setUrl($this->requestUrl->getUrl())
            ->setData($data)
            ->send();

        return $response;
    }

    /**
     * Send error message
     * @param string $message
     * @param string $responseMessage
     * @param string $action
     * @param array|null $requestParams
     */
    private function sendErrorMessage(string $message, string $responseMessage, string $action, ?array $requestParams)
    {
        $error = [
            'message'       => $message,
            'url'           => $this->requestUrl->getUrl(),
            'upup_action'   => $action,
            'httpResponse'  => $responseMessage,
            'requestParams' => $requestParams
        ];
        Yii::error($error, 'upup');
        /** @var $apiLogger \logger\ApiLogger */
        $apiLogger = \Yii::$app->apiLogger;
        $apiLogger->log(
            'Message: ' . $error['message'] .
            '; Url: ' . $error['url'] .
            '; HttpResponse: ' . $error['httpResponse'] .
            '; RequestParams: ' . implode(',', array_map_assoc(function ($k, $v) {
                return "$k=$v";
            }, $error['requestParams']))
        );
    }

    private function getCommonParams(string $action): array
    {
        return [
            'v'                     => $this->requestUrl->getVersion(),
            'key'                   => $this->requestUrl->getApikey(),
            'test'                  => (int)!$this->requestUrl->getShowAuto(),
            'percentPlacementOrder' => (int)$this->requestUrl->getRate(),
            'character'             => 'utf-8',
            'controller'            => 'order',
            'function'              => $action,
            'json'                  => 1,
        ];
    }
}