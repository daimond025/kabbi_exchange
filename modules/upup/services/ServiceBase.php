<?php


namespace app\modules\upup\services;


use Yii;
use yii\httpclient\Client;

class ServiceBase
{
    /**
     * @return Client
     */
    protected function getHttpClient()
    {
        /** @var Client $client */
        $client = Yii::$app->get('httpClient');

        return $client;
    }
}