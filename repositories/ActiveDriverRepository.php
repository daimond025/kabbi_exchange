<?php


namespace app\repositories;


use app\repositories\interfaces\IActiveDriverRepository;
use Yii;

class ActiveDriverRepository implements IActiveDriverRepository
{
    private $tenantId;

    public function __construct(int $tenantId)
    {
        $this->tenantId = $tenantId;
    }

    public function getAll()
    {
        return Yii::$app->redis_workers->executeCommand('hvals', [$this->tenantId]);
    }

    public function getOne(string $callsign)
    {
        return unserialize(Yii::$app->redis_workers->executeCommand('hget', [$this->tenantId, $callsign]));
    }

    public function store(string $callsign, $data)
    {
        return Yii::$app->redis_workers->executeCommand('hset', [$this->tenantId, $callsign, $data]);
    }
}