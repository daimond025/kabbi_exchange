<?php


namespace app\repositories;


use app\repositories\exceptions\NotFoundException;
use app\repositories\exceptions\WrongDataException;
use app\repositories\interfaces\IActiveOrders;
use Yii;

class ActiveOrdersRepository implements IActiveOrders
{
    /**
     * @param int $tenantId
     * @param int $orderId
     * @return array
     * @throws NotFoundException
     */
    public function getOne(int $tenantId, int $orderId): array
    {
        $serializedOrderData = Yii::$app->redis_orders->executeCommand('hget', [$tenantId, $orderId]);

        if (empty($serializedOrderData)) {
            throw new NotFoundException('The active order is not found');
        }

        $orderfData = unserialize($serializedOrderData);

        if ($orderfData === false) {
            throw new WrongDataException('Wrong serialized order data');
        }

        return $orderfData;
    }
}