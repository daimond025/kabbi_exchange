<?php


namespace app\repositories;


use app\models\worker\WorkerHasPosition;
use app\repositories\exceptions\NotFoundException;
use app\repositories\interfaces\IDocumentsRepository;
use Yii;
use yii\db\ActiveQuery;

class DocumentsRepository implements IDocumentsRepository
{
    /**
     * @param int $workerId
     * @return array The fields are consistent of tbl_worker_driver_license
     */
    public function getDriverLicense(int $workerId): array
    {
        $res = WorkerHasPosition::find()
            ->alias('wp')
            ->where([
                'wp.position_id' => Yii::$app->params['taxi_position_id'],
                'wp.worker_id'   => $workerId,
            ])
            ->with([
                'documents' => function (ActiveQuery $q) {
                    $q->joinWith([
                        'document' => function (ActiveQuery $document) {
                            $document->where(['code' => Yii::$app->params['driver_license_doc_code']]);
                        },
                    ], false);
                    $q->with('workerDriverLicenses');
                },
            ])
            ->limit(1)
            ->asArray()
            ->one();

        if (!isset($res['documents'][0]['workerDriverLicenses']) || empty($res['documents'][0]['workerDriverLicenses'])) {
            throw new NotFoundException('The driver license of worker_id = ' . $workerId . ' was not found');
        }

        return $res['documents'][0]['workerDriverLicenses'];
    }
}