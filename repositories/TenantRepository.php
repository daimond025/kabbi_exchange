<?php


namespace app\repositories;


use app\repositories\exceptions\NotFoundException;
use yii\db\Query;

class TenantRepository
{
    const TENANT_TABLE = '{{%tenant}}';

    /**
     * @param string $domain
     * @return false|null|string
     * @throws NotFoundException
     */
    public function getIdByDomain(string $domain)
    {
        $res = (new Query())->from(self::TENANT_TABLE)->where(['domain' => $domain])->select('tenant_id')->scalar();

        if (empty($res)) {
            throw new NotFoundException();
        }

        return $res;
    }
}