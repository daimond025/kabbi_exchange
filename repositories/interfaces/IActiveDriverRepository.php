<?php
namespace app\repositories\interfaces;

interface IActiveDriverRepository
{
    public function getAll();

    public function getOne(string $callsign);

    public function store(string $callsign, $data);
}