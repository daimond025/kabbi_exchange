<?php

namespace app\repositories\interfaces;

interface IActiveOrders
{
    public function getOne(int $tenantId, int $orderId): array;
}