<?php

namespace app\repositories\interfaces;

interface IDocumentsRepository
{
    public function getDriverLicense(int $workerId): array;
}